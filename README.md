# spectrumwordlistgen

A tool to create wordlists for cracking Spectrum WIFI router default passwords.

# usage

```
./make_spectrum_wordlist <outfile-root> <num_word0> <num_words1> <dictfile>
```

Provided is a `lowercase-english` wordlist pulled from `/use/share/dict/american-english`. 

Spectrum routers use a default password of the format:

```
<lowercase_word_1><lowercase_word_2><three_digit_number>
```

So for example:

```
rainbowcake123
bacontrumpet456
moonlander789
```

and etc...

# example usage

```
./make_spectrum_wordlist.sh spectrum-bruteforce 10 100 lowercase-english
```

`outfile-root` specifies the base filename of the outputted wordlists.

`num_word0` specifies the number of outer-words to randomly grab from `dictfile`.

`num_word1` specifies the number of inner-words to randomly grab from `dictfile`.

By default, this script will attempt to split the generated file into files containing 100000 lines each.

This can be useful if you wish to distribute the cracking to many machines, or if you only wish to run small batches at a time.

This can also be useful for eliminating which passwords you've tried already without re-running a job on the WHOLE list.

Naturally, who else is crazy enough to do CPU cracking in 2024?

**budget mage** :D 

[https://www.evildojo.com](https://www.evildojo.com)
[https://www.x.com/evildojo666](https://www.x.com/evildojo666)

